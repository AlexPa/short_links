# short_links
Это сервис, с помощью которого можно
уменьшить размер ссылки
(ее альтернативные названия — URL или линк).
После этого короткий адрес ссылки станет короче.
## Install

1. Собрать go проект

```
cd ./shorts/src
go build -o ./build/shortsExec ./cmd/shorts/main.go
```

2. Собрать vuejs

```
cd ./shorts/src/vuejs
yarn build
```