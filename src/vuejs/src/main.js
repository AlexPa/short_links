import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import axios from "axios";
import VueAxios from "vue-axios";
import BalmUI from 'balm-ui'; // Official Google Material Components
import BalmUIPlus from 'balm-ui-plus'; // BalmJS Team Material Components
import 'balm-ui-css';
import cfg from '../../config.yml'


const app = createApp(App)

app.use(router)
app.use(VueAxios, axios)
app.use(BalmUI);
app.use(BalmUIPlus);
app.mount('#app')

const axiosInstance = axios.create({
    withCredentials: true,
    baseURL: cfg.base_url
})
app.config.globalProperties.$axios = {...axiosInstance}
