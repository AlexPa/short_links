import {createRouter, createWebHistory} from "vue-router";
import Home from "@/views/HomeView.vue";
import CreateShort from "@/views/CreateShort.vue";
import ListShort from "@/views/ListShort.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/short/create",
        name: "Create short link",
        component: CreateShort,
    },
    {
        path: "/short/",
        name: "List of short link",
        component: ListShort,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
