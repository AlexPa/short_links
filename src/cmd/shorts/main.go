package main

import (
	"context"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"net/http"
	"shorts/internal/client/mongodb"
	"shorts/internal/config"
	"shorts/internal/handler"
	"shorts/internal/logger"
	"shorts/internal/short"
	mongodb2 "shorts/internal/short/db/mongodb"
)

var storage short.Storage

func main() {
	e := echo.New()

	standardLogger := e.Logger

	logRus := logger.GetLogger()

	logRus.SetLevel(logrus.DebugLevel)
	logRus.SetFormatter(&logrus.TextFormatter{})

	e.Logger = logger.MyLogger{}.NewLogger(logRus, standardLogger)

	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:    true,
		LogStatus: true,
		LogValuesFunc: func(c echo.Context, values middleware.RequestLoggerValues) error {
			logRus.WithFields(logrus.Fields{
				"URI":    values.URI,
				"status": values.Status,
			}).Info("request")

			return nil
		},
	}))
	storage = GetStorage(e)

	e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(config.GetConfig().RateLimit)))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     config.GetConfig().FriendlyUrls,
		AllowCredentials: true,
	}))
	e.GET("/", func(c echo.Context) error {
		c.Logger().Info(config.GetConfig().FriendlyUrls)
		return c.JSON(http.StatusOK, "Hello")
	})

	controller := handler.Controller{Storage: storage, NeedValidate: true}

	e.GET("/short/list", controller.List)
	e.POST("/short/create", controller.Create)
	e.GET("/s/:id", controller.RedirectByShort)

	e.Logger.Fatal(e.Start(":1323"))
}

func GetStorage(e *echo.Echo) short.Storage {
	cfg := config.GetConfig()
	storageClient := mongodb.NewClient(context.Background(), mongodb.MongoClientConfig{
		Host:     cfg.Mongodb.Host,
		Port:     cfg.Mongodb.Port,
		Username: cfg.Mongodb.Username,
		Password: cfg.Mongodb.Password,
		Database: cfg.Mongodb.Database,
		AuthDB:   cfg.Mongodb.AuthDb,
	})
	collection := storageClient.Collection(cfg.Mongodb.Collection)
	return mongodb2.NewStorage(collection, e.Logger)
}
