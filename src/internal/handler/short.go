package handler

import (
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	gonanoid "github.com/matoous/go-nanoid"
	"net/http"
	"shorts/internal/config"
	"shorts/internal/help"
	"shorts/internal/short"
	mongodb "shorts/internal/short/db/mongodb"
	"strconv"
	"time"
)

const PageLimit int = 10

type Controller struct {
	Storage      short.Storage
	NeedValidate bool
}

func (h *Controller) Create(c echo.Context) error {
	sh := short.Model{}

	// Fill struct from request body
	c.Bind(&sh)
	sh.Date = time.Now()
	sh.ShortPart = gonanoid.MustID(config.GetConfig().MaxLengthNanoId)
	c.Logger().Debug(sh)

	err := h.Validate(c, sh)
	if err != nil {
		c.Logger().Error(err)
		err := help.GetHumanReadableValidationMessages(err)
		return c.JSON(http.StatusBadRequest, err)
	}
	id, err := h.Storage.Create(sh)
	sh, err = h.Storage.GetById(id)

	return c.JSON(http.StatusCreated, sh)
}
func (h Controller) Validate(c echo.Context, sh short.Model) error {
	// Валидация какая-то похожая на говно :((
	if !h.NeedValidate {
		return nil
	}
	validate := validator.New()
	validate.RegisterValidation("existsInStorage", func(fl validator.FieldLevel) bool {
		value := fl.Field().String()
		if value == "" {
			return false
		}
		//TODO: Extract json name by Name of struct field
		//field := fl.FieldName()
		//c.Logger().Debug(fmt.Sprintf("Validating field: %s with value: %s", field, value))
		_, err := h.Storage.GetByShortPart(value)
		if err != nil && errors.Is(err, mongodb.NotFoundInStorage) {
			return true
		}
		return false
	})
	return validate.Struct(sh)
}
func (h *Controller) List(c echo.Context) error {
	page := 1
	startString := c.QueryParam("page")
	page, err := strconv.Atoi(startString)
	if err != nil {
		page = 1
	}
	pageSize := PageLimit
	c.Logger().Debug(fmt.Sprintf("startString: %s page: %d pageSize: %d", startString, page, pageSize))
	filter := short.Filter{
		Start: (page - 1) * pageSize,
		Limit: pageSize,
	}

	data := struct {
		Data     []short.Model `json:"data"`
		Total    int           `json:"total"`
		PageSize int           `json:"page_size"`
	}{
		Data:     h.Storage.GetList(filter),
		Total:    h.Storage.GetTotalAmount(filter),
		PageSize: pageSize,
	}
	return c.JSON(http.StatusOK, data)
}

func (h *Controller) RedirectByShort(c echo.Context) error {
	id := c.Param("id")
	sh, err := h.Storage.GetByShortPart(id)
	if err != nil && errors.Is(err, mongodb.NotFoundInStorage) {
		return c.JSON(http.StatusNotFound, id)
	}
	c.Logger().Debug(sh)
	return c.Redirect(http.StatusMovedPermanently, sh.GetUrlOriginal())
}
