package handler

//https://jskim1991.medium.com/go-building-an-application-using-echo-framework-with-tests-controller-e4ca1187478c
import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"shorts/internal/short"
	"strings"
	"testing"
	"time"
)

var createdId string = "string_id_must_be_here"
var testUrl string = "http://ya.ru"
var shortPart string = "G65Rk"

type MockStorage struct {
}

func (m MockStorage) GetByShortPart(part string) (short.Model, error) {
	return m.GetById(part)
}

func (m MockStorage) GetByField(value string, field string) (short.Model, error) {
	//TODO implement me
	panic("implement me5")
}

func (m MockStorage) GetById(id string) (short.Model, error) {
	return short.Model{
		Id:          createdId,
		UrlOriginal: testUrl,
		ShortPart:   shortPart,
		Date:        time.Time{},
		Url:         "",
	}, nil
}

func (m MockStorage) GetList(filter short.Filter) []short.Model {
	var list = []short.Model{}

	m1, _ := m.GetById(createdId)
	list = append(
		list,
		m1,
	)
	return list
}

func (m MockStorage) GetTotalAmount(filter short.Filter) int {
	return 1
}

func (m MockStorage) Update(id int, short short.Model) short.Model {
	//TODO implement me
	panic("implement me2")
}

func (m MockStorage) Delete(id int) bool {
	//TODO implement me
	panic("implement me1")
}

func (m MockStorage) Create(short short.Model) (string, error) {
	return createdId, nil
}

func TestCreateShort(t *testing.T) {
	e := echo.New()
	shortJSON := `{"url_original":"http://ya.ru"}`

	req := httptest.NewRequest(http.MethodPost, "/short/create", strings.NewReader(shortJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStorage := MockStorage{}

	controller := Controller{&mockStorage, false}

	if assert.NoError(t, controller.Create(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func TestRedirectByShort(t *testing.T) {
	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, "/s/"+shortPart, nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStorage := MockStorage{}

	controller := Controller{&mockStorage, false}

	if assert.NoError(t, controller.RedirectByShort(c)) {
		assert.Equal(t, http.StatusMovedPermanently, rec.Code)
	}
}

func TestList(t *testing.T) {
	e := echo.New()

	req := httptest.NewRequest(http.MethodPost, "/short/list", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mockStorage := MockStorage{}

	controller := Controller{&mockStorage, false}

	if assert.NoError(t, controller.List(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
