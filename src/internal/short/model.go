package short

import (
	"fmt"
	"shorts/internal/config"
	"time"
)

type Model struct {
	Id          string    `json:"id" bson:"_id,omitempty"`
	UrlOriginal string    `json:"url_original" bson:"url_original,omitempty" validate:"url,required,existsInStorage"`
	ShortPart   string    `json:"short_part" bson:"short_part,omitempty" validate:"required,existsInStorage"`
	Date        time.Time `json:"date" bson:"date,omitempty"`
	Url         string    `json:"url"`
}

func (m Model) GetRedirectUrl() string {
	return fmt.Sprintf("%s/s/%s", config.GetConfig().BaseUrl, m.ShortPart)
}

func (m Model) GetUrlOriginal() string {
	return m.UrlOriginal
}
