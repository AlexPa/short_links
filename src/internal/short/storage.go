package short

type Filter struct {
	Start int
	Limit int
}
type Storage interface {
	GetByShortPart(part string) (Model, error)
	GetByField(value string, field string) (Model, error)
	GetById(id string) (Model, error)
	GetList(filter Filter) []Model
	GetTotalAmount(filter Filter) int
	Create(short Model) (string, error)
	Update(id int, short Model) Model
	Delete(id int) bool
}
