package mongodb

import (
	"context"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"shorts/internal/short"
)

var NotFoundInStorage = errors.New("not found")

type MongoDatabase struct {
	collection *mongo.Collection
	ctx        context.Context
	logger     echo.Logger
}

func (d MongoDatabase) GetTotalAmount(filter short.Filter) int {
	var f = bson.M{}
	cnt, err := d.collection.CountDocuments(d.ctx, f)
	if err != nil {
		return 0
	}
	return int(cnt)
}

func (d MongoDatabase) GetByField(value string, field string) (short short.Model, err error) {
	//var filter = bson.M{"short_part": part}
	//result := d.collection.FindOne(d.ctx, filter)
	//if errors.Is(result.Err(), mongo.ErrNoDocuments) {
	//	return short, NotFoundInStorage
	//}
	//if result.Err() != nil {
	//	return short, err
	//}
	//err = result.Decode(&short)
	//if err != nil {
	//	return short, err
	//}
	//return short, nil
	return short, err
}

func (d MongoDatabase) GetList(filter short.Filter) (model []short.Model) {
	opt := options.Find()
	// Sort by `_id` field descending
	opt.SetSort(bson.D{{"date", -1}})
	// Limit by 10 documents only
	opt.SetLimit(int64(filter.Limit))
	opt.SetSkip(int64(filter.Start))

	var f = bson.M{}
	cursor, err := d.collection.Find(d.ctx, f, opt)
	if err != nil {
		return nil
	}
	if cursor.Err() != nil {
		return nil
	}
	err = cursor.All(d.ctx, &model)
	if err != nil {
		return nil
	}
	// Должен быть способ сделать это красиво
	for i, _ := range model {
		model[i].Url = model[i].GetRedirectUrl()
	}
	return model
}

func (d MongoDatabase) GetByShortPart(part string) (short short.Model, err error) {
	var filter = bson.M{"short_part": part}
	result := d.collection.FindOne(d.ctx, filter)
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return short, NotFoundInStorage
	}
	if result.Err() != nil {
		return short, err
	}
	err = result.Decode(&short)
	if err != nil {
		return short, err
	}
	return short, nil
}

func (d MongoDatabase) GetById(id string) (short short.Model, err error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		d.logger.Errorf("Can not create ObjectID from hex %s ", id)
		return short, err
	}
	var filter = bson.M{"_id": oid}
	result := d.collection.FindOne(d.ctx, filter)
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return short, NotFoundInStorage
	}
	if result.Err() != nil {
		return short, err
	}
	err = result.Decode(&short)
	if err != nil {
		return short, err
	}
	return short, nil
}

func (d MongoDatabase) Create(short short.Model) (string, error) {
	d.logger.Debug(short)
	res, err := d.collection.InsertOne(d.ctx, short)
	if err != nil {
		d.logger.Error(err)
		return "", fmt.Errorf("Can not create model because %s ", err)
	}
	id := res.InsertedID.(primitive.ObjectID)
	d.logger.Debugf("Model was created with id %s", id.Hex())
	return id.Hex(), nil
}

func (d MongoDatabase) Update(id int, short short.Model) short.Model {
	//TODO implement me
	panic("implement me")
}

func (d MongoDatabase) Delete(id int) bool {
	//TODO implement me
	panic("implement me")
}

func NewStorage(collection *mongo.Collection, logger echo.Logger) short.Storage {
	return MongoDatabase{
		collection: collection,
		ctx:        context.Background(),
		logger:     logger,
	}
}
