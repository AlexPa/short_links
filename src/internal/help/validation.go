package help

import (
	"errors"
	"github.com/go-playground/validator/v10"
)

type ValidationError struct {
	Field string
	Msg   string
}

func MsgForTag(tag string) string {
	switch tag {
	case "required":
		return "This field is required"
	case "email":
		return "Invalid email"
	case "url":
		return "Wrong format of url"
	}
	return ""
}

func GetHumanReadableValidationMessages(err error) []ValidationError {
	var ve validator.ValidationErrors
	var out []ValidationError
	if errors.As(err, &ve) {
		out = make([]ValidationError, len(ve))
		for i, fe := range ve {
			out[i] = ValidationError{fe.Field(), MsgForTag(fe.Tag())}
		}
	}
	return out
}
