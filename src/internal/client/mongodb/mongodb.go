package mongodb

import (
	"context"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type MongoClientConfig struct {
	Host, Port, Username, Password, Database, AuthDB string
}

func NewClient(ctx context.Context, cfg MongoClientConfig) *mongo.Database {
	var mongoDBURL string

	var opts *options.ClientOptions
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)

	if cfg.Username == "" || cfg.Password == "" {
		mongoDBURL = fmt.Sprintf("mongodb://%s:%s", cfg.Host, cfg.Port)
		opts = options.Client().ApplyURI(mongoDBURL).SetServerAPIOptions(serverAPI)
	} else {
		mongoDBURL = fmt.Sprintf("mongodb://%s:%s@%s:%s", cfg.Username, cfg.Password, cfg.Host, cfg.Port)
		cred := options.Credential{
			AuthMechanism: "SCRAM-SHA-256",
			AuthSource:    cfg.AuthDB,
			Username:      cfg.Username,
			Password:      cfg.Password,
		}
		fmt.Println(mongoDBURL)
		spew.Dump(cred)
		opts = options.Client().ApplyURI(mongoDBURL).SetServerAPIOptions(serverAPI).SetAuth(cred)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		panic(fmt.Errorf("failed to connect to mongo error is %f", err))
	}

	if err = client.Ping(ctx, nil); err != nil {
		panic(fmt.Errorf("failed to ping %f", err))
	}
	return client.Database(cfg.Database)
}
