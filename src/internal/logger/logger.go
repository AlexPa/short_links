package logger

import (
	"github.com/labstack/echo/v4"
	log2 "github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
	"io"
)

type MyLogger struct {
	logrusLogger   *logrus.Logger
	standardLogger echo.Logger
}

func (l MyLogger) Output() io.Writer {
	return l.standardLogger.Output()
}

func (l MyLogger) SetOutput(w io.Writer) {
	l.logrusLogger.SetOutput(w)
}

func (l MyLogger) Prefix() string {
	return l.standardLogger.Prefix()
}

func (l MyLogger) SetPrefix(p string) {
	l.standardLogger.SetPrefix(p)
}

func (l MyLogger) Level() log2.Lvl {
	return l.standardLogger.Level()
}

func (l MyLogger) SetLevel(v log2.Lvl) {
	l.standardLogger.SetLevel(v)
}

func (l MyLogger) SetHeader(h string) {
	l.standardLogger.SetHeader(h)
}

func (l MyLogger) Print(i ...interface{}) {
	l.logrusLogger.Print(i...)
}

func (l MyLogger) Printf(format string, args ...interface{}) {
	l.logrusLogger.Printf(format, args...)
}

func (l MyLogger) Printj(j log2.JSON) {
	l.standardLogger.Printj(j)
}

func (l MyLogger) Debug(i ...interface{}) {
	l.logrusLogger.Debug(i...)
}

func (l MyLogger) Debugf(format string, args ...interface{}) {
	l.logrusLogger.Debugf(format, args...)
}

func (l MyLogger) Debugj(j log2.JSON) {
	l.standardLogger.Debugj(j)
}

func (l MyLogger) Info(i ...interface{}) {
	l.logrusLogger.Info(i...)
}

func (l MyLogger) Infof(format string, args ...interface{}) {
	l.logrusLogger.Infof(format, args...)
}

func (l MyLogger) Infoj(j log2.JSON) {
	l.standardLogger.Infoj(j)
}

func (l MyLogger) Warn(i ...interface{}) {
	l.logrusLogger.Warn(i...)
}

func (l MyLogger) Warnf(format string, args ...interface{}) {
	l.logrusLogger.Warnf(format, args...)
}

func (l MyLogger) Warnj(j log2.JSON) {
	l.standardLogger.Warnj(j)
}

func (l MyLogger) Error(i ...interface{}) {
	l.logrusLogger.Error(i...)
}

func (l MyLogger) Errorf(format string, args ...interface{}) {
	l.logrusLogger.Errorf(format, args...)
}

func (l MyLogger) Errorj(j log2.JSON) {
	l.standardLogger.Errorj(j)
}

func (l MyLogger) Fatal(i ...interface{}) {
	l.logrusLogger.Fatal(i...)
}

func (l MyLogger) Fatalj(j log2.JSON) {
	l.standardLogger.Fatalj(j)
}

func (l MyLogger) Fatalf(format string, args ...interface{}) {
	l.logrusLogger.Fatalf(format, args...)
}

func (l MyLogger) Panic(i ...interface{}) {
	l.logrusLogger.Panic(i...)
}

func (l MyLogger) Panicj(j log2.JSON) {
	l.standardLogger.Panicj(j)
}

func (l MyLogger) Panicf(format string, args ...interface{}) {
	l.logrusLogger.Panicf(format, args...)
}

func (l MyLogger) NewLogger(logrusLogger *logrus.Logger, standardLogger echo.Logger) echo.Logger {
	return MyLogger{
		logrusLogger:   logrusLogger,
		standardLogger: standardLogger,
	}
}
