package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/time/rate"
	"sync"
)

type Config struct {
	IsDebug         *bool      `yaml:"is_debug"`
	MaxLengthNanoId int        `yaml:"max_length_nano_id"`
	RateLimit       rate.Limit `yaml:"rate_limit"`
	BaseUrl         string     `yaml:"base_url"`
	FriendlyUrls    []string   `yaml:"friendly_urls"`
	Mongodb         struct {
		Host       string `yaml:"host"`
		Port       string `yaml:"port"`
		Database   string `yaml:"database"`
		Username   string `yaml:"username"`
		Password   string `yaml:"password"`
		AuthDb     string `yaml:"auth_db"`
		Collection string `yaml:"collection"`
	} `yaml:"mongodb"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {
		middleware.Logger()
		//logger := logging.GetLogger()
		//logger.Info("Reading config")
		instance = &Config{}
		err := cleanenv.ReadConfig("config.yml", instance)
		if err != nil {
			_, _ = cleanenv.GetDescription(instance, nil)
			//logger.Info(text)
			//logger.Fatal("Can't read config")
		}
	})
	return instance
}
